#!/bin/bash
yum install -y https://repo.saltstack.com/yum/redhat/salt-repo-latest-1.el7.noarch.rpm
yum install -y salt-minion
echo 'master: salt.xc-inc.com' > /etc/salt/minion
systemctl enable salt-minion.service
systemctl start salt-minion.service

